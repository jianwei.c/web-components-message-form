## Usage

### Installation
```
npm install --save git@gitlab.com:affinidi/safe-travel/issuance-and-labs/research/web-components-message-form.git
```

### In an html file
```html
<html>
  <head>
    <script type="module">
      import 'web-components-message-form.js';
    </script>
  </head>
  <body>
        <message-form id="message-form-id" messageInput="message-default-input">
        </message-form>
  </body>
</html>
```
By default, a native `<button>` element (or `input type="submit"`) will submit this form.
However, if you want to submit it from a custom element's click handler, you need to explicitly
call the `message-form`'s `submit` method:

```html
  <message-form raised onclick="submitForm()">Submit</message-form>

  function submitForm() {
    document.getElementById('message-form-id').submit();
  }
```

## Contributing
If you want to send a PR to this element, here are
the instructions for running the tests and demo locally:

### Installation
```sh
git clone git@gitlab.com:affinidi/safe-travel/issuance-and-labs/research/web-components-message-form.git
cd web-components-message-form
npm install -g polymer-cli
```

### Running the demo locally
```sh
polymer serve --npm
open http://127.0.0.1:<port>/demo/
```
