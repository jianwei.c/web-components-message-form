class Modal extends HTMLElement {
    
  get messageInput() {
    return this.getAttribute('messageInput');
  }

  set messageInput(value) {
    this.setAttribute('messageInput', value);
  }

  constructor() {
    super();
  }

  connectedCallback() {
    this._render();
    this._attachEventHandlers();
  }


  _render() {
    const container = document.createElement("div");
    container.innerHTML = `
        <form id="${this.id}"
        <label for="messageInput">Message:</label><br>
        <input type="text" id="messageInput" name="messageInput" value="${this.messageInput}"><br>
        <input class='submit' type="submit" value="Submit">
        </form>`;

    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(container);
  }

  _attachEventHandlers() {
    const okButton = this.shadowRoot.querySelector(".submit");
    okButton.addEventListener('click', e => {
      this.dispatchEvent(new CustomEvent("submit"))
    });
  }
}
window.customElements.define('message-form', Modal);